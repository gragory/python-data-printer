#!/usr/bin/env python
# coding: utf-8

import unittest
from dataprinter import DataPrinter, _uncolored, DataPrinterColor
import os
import sys

if sys.version_info[0] < 3:
    from StringIO import StringIO
else:
    from io import StringIO

class TestDataPrinter(unittest.TestCase):
    def test_init_color(self):
        with self.assertRaises(TypeError):
            DataPrinter(color = 'not_dict')
        with self.assertRaises(ValueError):
            DataPrinter(color = {'unknown_color': 1})
        with self.assertRaises(TypeError):
            DataPrinter(color = {'caller_info' : 'not DataPrinterColor'})

    def test_return_value(self):
        val  = 123
        dumper = DataPrinter(output = os.devnull)
        dumper.return_value = 'pass'
        self.assertEqual(dumper.p(val), val)
        dumper.return_value = 'void'
        self.assertEqual(dumper.p(val), None)
        dumper.return_value = 'dump'
        dump = dumper.np(val)
        self.assertEqual(dumper.p(val), dump)

    def test_output_end(self):
        result = StringIO()
        dumper = DataPrinter(
            return_value = 'dump',
            output       = result,
            output_end   = 'END',
        )
        dump = dumper.p('test')
        self.assertEqual(result.getvalue(), dump + 'END')

    def test_colored(self):
        dumper = DataPrinter(caller_info = 1)
        dumper.colored = True
        dump = dumper.np(123)
        self.assertNotEqual(dump, _uncolored(dump))
        dumper.colored = False
        dump = dumper.np(123)
        self.assertEqual(dump, _uncolored(dump))

    def test_colorize(self):
        dumper = DataPrinter()
        dumper.color = {'test': DataPrinterColor('red')}
        string = 'test'
        dumper.colored = False
        colored_string = dumper.colorize('test', string)
        self.assertEqual(string, colored_string)
        dumper.colored = True
        with self.assertRaises(ValueError):
            dumper.colorize('unknown_color', string)
        colored_string = dumper.colorize('test', string)
        expected = DataPrinterColor('red').colored(string)
        self.assertEqual(colored_string, expected)

    def test_escape(self):
        dumper = DataPrinter()
        dumper.colored = False
        dumper.escape_chars = 'all'
        expected = u'\\x31\\x32\\x33'
        dumper.escape_chars = 'nonlatin1'
        dumper.print_escapes = True
        dumper.scalar_quotes = "'"
        expected = '\\xd1\\x8b\\xd1\\x8b\\xd1\\x8b sss \\n\\xd0\\xb2\\\''
        self.assertEqual(expected, dumper.escape(u'ыыы sss \nв\'', 'test'))

if __name__ == '__main__':
    unittest.main()
