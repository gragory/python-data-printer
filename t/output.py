#!/usr/bin/env python
# coding: utf-8
from __future__ import print_function

import unittest
from dataprinter import DataPrinterOutput
import os
import sys
import tempfile


if sys.version_info[0] < 3:
    from StringIO import StringIO
else:
    from io import StringIO

TEST_STR = 'test str'


class TestDataPrinter(unittest.TestCase):

    def test_stdout(self):
        old_stdout = sys.stdout
        result = StringIO()
        sys.stdout = result
        with DataPrinterOutput(output='stdout'):
            print(TEST_STR, end='')
        sys.stdout = old_stdout
        self.assertEqual(result.getvalue(), TEST_STR)

    def test_stderr(self):
        old_stderr = sys.stderr
        result = StringIO()
        sys.stderr = result
        with DataPrinterOutput(output='stderr'):
            print(TEST_STR, end='')
        sys.stderr = old_stderr
        self.assertEqual(result.getvalue(), TEST_STR)

    def test_not_str_output(self):
        result = StringIO()
        with DataPrinterOutput(output=result):
            print(TEST_STR, end='')
        self.assertEqual(result.getvalue(), TEST_STR)

    def test_str_output(self):
        f = tempfile.NamedTemporaryFile(delete=False)
        with DataPrinterOutput(output=f.name):
            print(TEST_STR, end='')
        f.seek(0)
        content = f.read().decode("utf-8")
        f.close()
        os.unlink(f.name)
        self.assertEqual(content, TEST_STR)


if __name__ == '__main__':
    unittest.main()
