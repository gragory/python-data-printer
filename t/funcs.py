#!/usr/bin/env python
# coding: utf-8

import unittest
import dataprinter
import sys


class TestFunctions(unittest.TestCase):

    def test_ansi_escape_removement(self):
        self.assertEqual(dataprinter._uncolored('\x1b[31m123\x1b[0m'), '123')

    def test_is_unicode(self):
        self.assertTrue(dataprinter._is_unicode(u'ыы'))
        if sys.version_info[0] < 3:
            self.assertFalse(dataprinter._is_unicode(bytes('ыы')))
            self.assertFalse(dataprinter._is_unicode(bytearray('ыы')))
        else:
            self.assertFalse(dataprinter._is_unicode(bytes('ыы'.encode('utf-8'))))
            self.assertFalse(dataprinter._is_unicode(bytearray('ыы'.encode('utf-8'))))

    def test_convert_to_unicode(self):
        expected=u'ыыы'
        if sys.version_info[0] < 3:
            got = dataprinter._convert_to_unicode('ыыы')
            self.assertEqual(got, expected)
            got = dataprinter._convert_to_unicode(bytearray('ыыы'))
            self.assertEqual(got, expected)
            got = dataprinter._convert_to_unicode(u'ыыы')
            self.assertEqual(got, expected)
        else:
            got = dataprinter._convert_to_unicode(bytes('ыыы'.encode('utf-8')))
            self.assertEqual(got, expected)
            got = dataprinter._convert_to_unicode(bytearray('ыыы'.encode('utf-8')))
            self.assertEqual(got, expected)
            got = dataprinter._convert_to_unicode(u'ыыы')
            self.assertEqual(got, expected)

    def test_full_escape(self):
        expected = u'\\x31\\x32\\x33\\xd1\\x8b'
        self.assertEqual(expected, dataprinter._full_escape(u'123ы'))

    def test_is_escape_char(self):
        self.assertTrue(dataprinter._is_escape_char('\\', 'none', True, "'"))
        self.assertTrue(dataprinter._is_escape_char("'", 'none', True, "'"))
        self.assertTrue(dataprinter._is_escape_char("\n", 'none', True, "'"))
        self.assertFalse(dataprinter._is_escape_char("\n", 'none', False, "'"))
        self.assertFalse(dataprinter._is_escape_char("x", 'none', False, "'"))
        self.assertFalse(dataprinter._is_escape_char(u"ы", 'none', False, "'"))
        self.assertTrue(dataprinter._is_escape_char(u"ы", 'nonascii', False, "'"))
        self.assertTrue(dataprinter._is_escape_char(u"ы", 'nonlatin1', False, "'"))

    def test_escape_generator(self):
        generator = dataprinter._escape_generator(
            u'ыыы sss \nв\\\' dsas asdf asd\t\t',
            'nonlatin1', True, "'",
        )
        expected = [
            ('\\xd1\\x8b\\xd1\\x8b\\xd1\\x8b', True),
            (' sss ', False),
            ("\\n\\xd0\\xb2\\\\\\'", True),
            (' dsas asdf asd', False),
            ('\\t\\t', True),
        ]
        self.assertEqual(expected, [s for s in generator])

    def test_sorted_any(self):
        unsort = ['d', 1, 'b', 4, 2, 'a', 'c', 3]
        sort   = [1, 2, 3, 4, 'a', 'b', 'c', 'd']
        self.assertEqual(sort, [s for s in dataprinter._sorted_any(unsort)])

if __name__ == '__main__':
    unittest.main()
