#!/usr/bin/env python
# coding: utf-8

import unittest
from dataprinter import DataPrinterColor


class TestDataPrinterColor(unittest.TestCase):

    def test_colored(self):
        red = DataPrinterColor('red')
        self.assertEqual(red.colored(123), '\x1b[31m123\x1b[0m')


if __name__ == '__main__':
    unittest.main()
