#!/usr/bin/env python
# coding: utf-8

import unittest
from dataprinter import DataPrinterStack


class TestDataPrinterStack(unittest.TestCase):

    def test_push(self):
        stack = DataPrinterStack()
        stack.push('dump', 1)
        stack.push('dump', 2)
        stack.push('dump', 3)
        with self.assertRaises(ValueError):
            stack.push('bad operation not from operations', 1)
        self.assertEqual([i[1] for i in stack], [1, 2, 3])

    def test_unshift(self):
        stack = DataPrinterStack()
        stack.unshift('dump', 1)
        stack.unshift('dump', 2)
        stack.unshift('dump', 3)
        with self.assertRaises(ValueError):
            stack.unshift('bad operation not from operations', 1)
        self.assertEqual([i[1] for i in stack], [3, 2, 1])

    def test_pop(self):
        stack = DataPrinterStack()
        stack.push('dump', 1)
        stack.push('dump', 2)
        stack.push('dump', 3)
        (operation, item) = stack.pop()
        self.assertEqual(operation, 'dump')
        self.assertEqual(item, 3)
        self.assertEqual([i[1] for i in stack], [1, 2])

    def test_shift(self):
        stack = DataPrinterStack()
        stack.push('dump', 1)
        stack.push('dump', 2)
        stack.push('dump', 3)
        (operation, item) = stack.shift()
        self.assertEqual(operation, 'dump')
        self.assertEqual(item, 1)
        self.assertEqual([i[1] for i in stack], [2, 3])

    def test_push_stack(self):
        stack = DataPrinterStack()
        stack.push('dump', 1)
        stack.push('dump', 2)
        stack_2 = DataPrinterStack()
        stack_2.push('dump', 3)
        stack_2.push('dump', 4)
        stack.push_stack(stack_2)
        self.assertEqual([i[1] for i in stack], [1, 2, 3, 4])

    def test_unshift_stack(self):
        stack = DataPrinterStack()
        stack.push('dump', 1)
        stack.push('dump', 2)
        stack_2 = DataPrinterStack()
        stack_2.push('dump', 3)
        stack_2.push('dump', 4)
        stack.unshift_stack(stack_2)
        self.assertEqual([i[1] for i in stack], [3, 4, 1, 2])

    def test_process(self):
        stack = DataPrinterStack()
        stack.push('dump', 1)
        stack.push('dump', 2)
        stack.push('dump', 3)
        x = 0
        for (operation, item) in stack.process():
            self.assertEqual(operation, 'dump')
            if x == 0:
                self.assertEqual(item, 1)
                self.assertEqual([i[1] for i in stack], [2, 3])
                stack.push('dump', 4)
            elif x == 1:
                self.assertEqual(item, 2)
                self.assertEqual([i[1] for i in stack], [3, 4])
                stack.unshift('dump', 2)
            elif x == 2:
                self.assertEqual(item, 2)
                self.assertEqual([i[1] for i in stack], [3, 4])
                stack.push('dump', 5)
                stack.push('dump', 6)
            elif x == 3:
                self.assertEqual(item, 3)
                self.assertEqual([i[1] for i in stack], [4, 5, 6])
                stack.pop()
                stack.shift()
            elif x == 4:
                self.assertEqual(item, 5)
                self.assertFalse(stack)
            x += 1
        self.assertEqual(x, 5)


if __name__ == '__main__':
    unittest.main()
