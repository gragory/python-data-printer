Port of perl's `Data::Printer`_ to python
=========================================

DESCRIPTION
-----------

This is port of perl's `Data::Printer`_ to python.

**Many thanks to Breno G. de Oliveira <garu@cpan.org> for the Data::Printer module**

LICENSE AND COPYRIGHT
---------------------
Copyright 2017 Grigoriy Koudrenko "<gragory.mail@gmail.com>".

This module is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This module is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

The full text of the license can be found in the LICENSE file included with
this program.


.. _`Data::Printer`: https://metacpan.org/pod/Data::Printer


