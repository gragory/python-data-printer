#!/usr/bin/env python
# coding: utf-8
'''Port of perl's `Data::Printer`_ to python.'''

from __future__ import print_function
import termcolor
import re
import inspect
import os
import sys
from collections import defaultdict
from pprint import pprint

VERSION = '0.1'
ANSI_ESCAPE = re.compile('\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]')
STRING_BASES=(str, unicode if sys.version_info[0] < 3 else bytes)
PRINT_ESCAPES_MAP = {
    "\n" : "\\n",
    "\r" : "\\r",
    "\t" : "\\t",
    "\f" : "\\f",
    "\b" : "\\b",
    "\a" : "\\a",
}

def p(item, **kwargs):
    return DataPrinter(**kwargs).p(item)


def np(item, **kwargs):
    return DataPrinter(**kwargs).np(item)


def _uncolored(s):
    return ANSI_ESCAPE.sub('', s)

def _is_unicode(s):
    if sys.version_info[0] < 3:
        return isinstance(s, unicode)
    else:
        return not isinstance(s, (bytes, bytearray))

def _convert_to_unicode(s):
    if sys.version_info[0] < 3:
        return s if _is_unicode(s) else unicode(s.decode('utf-8'))
    else:
        return s if _is_unicode(s) else s.decode('utf-8')

def _full_escape(s):
    res = u''
    for char in s.encode('utf-8'):
        if sys.version_info[0] < 3:
            char = ord(char)
        res += '\\x{:02x}'.format(char)
    return res

def _is_escape_char(char, escape_chars, print_escapes, scalar_quotes):
    return (
        escape_chars == 'nonascii' and ord(char) > 128
        or escape_chars == 'nonlatin1' and ord(char) > 255
        or print_escapes and char in PRINT_ESCAPES_MAP
        or char == '\\'
        or char == scalar_quotes
    )


def _escape_generator(s, escape_chars, print_escapes, scalar_quotes):
    buff = u''
    escaped = False
    for char in s:
        if _is_escape_char(char, escape_chars, print_escapes, scalar_quotes):
            if not escaped and buff:
                yield (buff, escaped)
                buff = u''
            escaped = True
            if print_escapes and char in PRINT_ESCAPES_MAP:
                buff += PRINT_ESCAPES_MAP[char]
            elif (char == '\\'):
                buff += '\\\\'
            elif (char == scalar_quotes):
                buff += '\\' + scalar_quotes
            else:
                buff += _full_escape(char)
        else:
            if escaped and buff:
                yield (buff, escaped)
                buff = u''
            escaped = False
            buff += char
    if (buff):
        yield (buff, escaped)

def _sorted_any(item):
    types = defaultdict(list)
    for i in item:
        types[str(type(i))].append(i)
    for i in sorted(types):
        for j in sorted(types[i]):
            yield j

class DataPrinterColor:

    def __init__(self, color=None, on_color=None, attrs=None):
        self.color = color
        self.on_color = on_color
        self.attrs = attrs

    def colored(self, s):
        return termcolor.colored(s, **self.__dict__)

class DataPrinterStack (list):
    operations = {'dump', 'print', 'set_depth', 'set_indent', 'set_path'}

    def push(self, operation, item):
        if operation not in self.operations:
            raise ValueError(
                "operation must be one of " + str(tuple(self.operations))
            )
        stack_element = (operation, item)
        self.append(stack_element)

    def unshift(self, operation, item):
        if operation not in self.operations:
            raise ValueError(
                "operation must be one of " + str(tuple(self.operations))
            )
        stack_element = (operation, item)
        self.insert(0, stack_element)

    def pop(self):
        return super(DataPrinterStack, self).pop()

    def shift(self):
        return super(DataPrinterStack, self).pop(0)

    def push_stack(self, stack):
        for i in stack:
            self.push(*i)

    def unshift_stack(self, stack):
        for i in stack[::-1]:
            self.unshift(*i)

    def process(self):
        while(self):
            yield self.shift()

class DataPrinterOutput:
    def __init__(self, output=None):
        if isinstance(output, STRING_BASES):
            if output == 'stdout':
                output = sys.stdout
            elif output == 'stderr':
                output = sys.stderr
        elif not hasattr(output, 'write'):
            raise TypeError("output must must has attribute 'write'")
        self.output = output
        self.opened = False

    def __enter__(self):
        if isinstance(self.output, STRING_BASES):
            self.output = open(self.output, 'w')
            self.opened = True
        self.old_stdout = sys.stdout
        self.old_stdout.flush()
        sys.stdout = self.output

    def __exit__(self, exc_type, exc_value, traceback):
        self.output.flush()
        if self.opened:
            self.output.close()
        sys.stdout = self.old_stdout

    def isatty(self):
        if not hasattr(self.output, 'fileno'):
            return False
        try:
            return os.isatty(self.output.fileno())
        except:
            return False

### DEFAULT FILTERS ###
def none_filter(dumper, item):
    dumper.stack.unshift('print', dumper.colorize('none', 'None'))

def bool_filter(dumper, item):
    if item:
        dumper.stack.unshift('print', dumper.colorize('true', 'True'))
    else:
        dumper.stack.unshift('print', dumper.colorize('false', 'False'))

def number_filter(dumper, item):
    dumper.stack.unshift('print', dumper.colorize('number', str(item)))

def number_long_filter(dumper, item):
    dumper.stack.unshift('print', dumper.colorize('number', str(item) + 'L'))

def string_filter(dumper, item):
    string = _convert_to_unicode(item)
    string = dumper.escape(string, 'string')
    string = dumper.scalar_quotes + string + dumper.scalar_quotes
    if dumper.show_unicode and _is_unicode(item):
        string = dumper.colorize('unicode', 'U') + string
    if dumper.show_bytes and isinstance(item, bytes):
        string = dumper.colorize('bytes', 'B') + string
    if dumper.show_bytearray and isinstance(item, bytearray):
        string = dumper.colorize('bytearray', '(bytearray)') + string
    dumper.stack.unshift('print', string)

def array_filter(dumper, item):
    stack = DataPrinterStack()
    dumper._depth += 1
    stack.push('set_depth', dumper._depth)
    start = '['
    stop  = ']'
    if isinstance(item, tuple):
        start = '('
        stop  = ')'
    if dumper.max_depth and dumper._depth > dumper.max_depth:
        stack.push('print', start + ' ... ' + stop)
    elif not item:
        stack.push('print', start + stop)
    else:
        stack.push('print', start + dumper._linebreak)
        dumper._current_indent += dumper.indent
        stack.push('set_indent', dumper._current_indent)
        ln  = len(str(len(item)-1))
        fmt = '{:' + str(ln+3) + 's}'
        old_path = dumper._path
        for i, val in enumerate(item):
            path = list(old_path)
            path.append('[' + str(i) + ']')
            stack.push('set_path', path)
            stack.push('print', ' ' * dumper._current_indent)
            if dumper.index:
                index = dumper.colorize('array', fmt.format('['+str(i)+']'))
                stack.push('print', index)
            stack.push('dump', val)
            if i < len(item)-1 or dumper.end_separator:
                stack.push('print', dumper.separator)
            stack.push('print', dumper._linebreak)
        stack.push('set_path', old_path)
        dumper._current_indent -= dumper.indent
        stack.push('set_indent', dumper._current_indent)
        stack.push('print', ' ' * dumper._current_indent + stop)
    dumper._depth -= 1
    stack.push('set_depth', dumper._depth)
    dumper.stack.unshift_stack(stack)

def range_filter(dumper, item):
    dumper.stack.unshift('print', dumper.colorize('range', str(item)))

def set_filter(dumper, item):
    stack = DataPrinterStack()
    dumper._depth += 1
    stack.push('set_depth', dumper._depth)
    start = 'set(['
    stop  = '])'
    start = dumper.colorize('set', start)
    stop  = dumper.colorize('set', stop)
    if isinstance(item, frozenset):
        start = 'frozenset(['
        stop  = '])'
    if dumper.max_depth and dumper._depth > dumper.max_depth:
        stack.push('print', start + ' ... ' + stop)
    elif not item:
        stack.push('print', start + stop)
    else:
        stack.push('print', start + dumper._linebreak)
        dumper._current_indent += dumper.indent
        stack.push('set_indent', dumper._current_indent)
        i = 0
        for val in _sorted_any(item) if dumper.sort_keys else item:
            stack.push('print', ' ' * dumper._current_indent)
            stack.push('dump', val)
            if i < len(item)-1 or dumper.end_separator:
                stack.push('print', dumper.separator)
            stack.push('print', dumper._linebreak)
            i += 1
        dumper._current_indent -= dumper.indent
        stack.push('set_indent', dumper._current_indent)
        stack.push('print', ' ' * dumper._current_indent + stop)
    dumper._depth -= 1
    stack.push('set_depth', dumper._depth)
    dumper.stack.unshift_stack(stack)

def dict_filter(dumper, item):
    stack = DataPrinterStack()
    dumper._depth += 1
    stack.push('set_depth', dumper._depth)
    if dumper.max_depth and dumper._depth > dumper.max_depth:
        stack.push('print', '{ ... }')
    elif not item:
        stack.push('print', '{}')
    else:
        stack.push('print', '{' + dumper._linebreak)
        dumper._current_indent += dumper.indent
        stack.push('set_indent', dumper._current_indent)
        ln_max = 0
        keys = []
        for key in _sorted_any(item) if dumper.sort_keys else item:
            formated = u''
            if isinstance(key, STRING_BASES):
                string = _convert_to_unicode(key)
                string = dumper.escape(string, 'dict')
                quotes = dumper.colorize('dict', dumper.scalar_quotes)
                string = quotes + string + quotes
                if dumper.show_unicode and _is_unicode(key):
                    string = dumper.colorize('unicode', 'U') + string
                if dumper.show_bytes and isinstance(key, bytes):
                    string = dumper.colorize('bytes', 'B') + string
                formated = string
            else:
                formated = dumper.colorize('dict', str(key))
            ln = len(_uncolored(formated))
            if ln > ln_max:
                ln_max = ln
            keys.append((formated, ln, key))
        i = 0;
        old_path = dumper._path
        for (formated, ln, key) in keys:
            path = list(old_path)
            path.append('[' + _uncolored(formated) + ']')
            stack.push('set_path', path)
            stack.push('print', ' ' * dumper._current_indent)
            stack.push('print', formated)
            if dumper.align_hash:
                stack.push('print', ' ' * (ln_max - ln))
            stack.push('print', dumper.dict_separator)
            stack.push('dump', item[key])
            if i < len(item)-1 or dumper.end_separator:
                stack.push('print', dumper.separator)
            stack.push('print', dumper._linebreak)
            i += 1
        stack.push('set_path', old_path)
        dumper._current_indent -= dumper.indent
        stack.push('set_indent', dumper._current_indent)
        stack.push('print', ' ' * dumper._current_indent + '}')
    dumper._depth -= 1
    stack.push('set_depth', dumper._depth)
    dumper.stack.unshift_stack(stack)

def function_filter(dumper, item):
    if not dumper.deparse:
        return dumper.stack.unshift('print', dumper.colorize('code', 'def func(): ...'))
    try:
        code = inspect.getsource(item)
        stack = DataPrinterStack()
        i = 0
        for l in code.splitlines():
            formated = dumper.colorize('code', l)
            if i:
                formated = (dumper.indent + dumper._current_indent) * ' ' + formated
            formated += '\n'
            stack.push('print', formated)
            i+=1
        dumper.stack.unshift_stack(stack)
    except:
        dumper.stack.unshift('print', dumper.colorize('code', 'def func(): ...'))

class DataPrinter:
    name           = 'var'
    caller_info    = False    # include information on what's being printed
    caller_message = 'Printing in line {line} of {filename}:'
    colored        = 'auto'
    return_value   = 'pass'   # also dump or void
    output         = 'stderr' # where to print the output
    output_end     = "\n"     # str that will be passed to print's end
    multiline      = True     # display in multiple lines
    escape_chars   = 'none'
    print_escapes  = False
    scalar_quotes  = '"'
    show_unicode   = sys.version_info[0] < 3
    show_bytes     = sys.version_info[0] >= 3
    show_bytearray = 1
    max_depth      = 0
    indent         = 4
    index          = True
    end_separator  = 0
    separator      = ','
    sort_keys      = True
    align_hash     = True
    dict_separator = ' : '
    deparse        = False

    color = {
        'caller_info' : DataPrinterColor('cyan', None, ['bold']),
        'none'        : DataPrinterColor('red', None, ['bold']),
        'true'        : DataPrinterColor('green'),
        'false'       : DataPrinterColor('red'),
        'range'       : DataPrinterColor('green'),
        'code'        : DataPrinterColor('green'),
        'number'      : DataPrinterColor('blue', None, ['bold']),
        'escaped'     : DataPrinterColor('red', None, ['bold']),
        'string'      : DataPrinterColor('yellow', None, ['bold']),
        'unicode'     : DataPrinterColor('yellow', None, ['bold']),
        'bytes'       : DataPrinterColor('yellow', None, ['bold']),
        'bytearray'   : DataPrinterColor('yellow', None, ['bold']),
        'array'       : DataPrinterColor('white', None, ['bold']),
        'repeated'    : DataPrinterColor('white', 'on_red'),
        'set'         : DataPrinterColor('cyan', None ),
        'dict'        : DataPrinterColor('magenta'),
        'unknown'     : DataPrinterColor('yellow', 'on_blue', ['bold']),
    }

    filters = {
        type(None) : none_filter,
        bool       : bool_filter,
        int        : number_filter,
        float      : number_filter,
        complex    : number_filter,
        str        : string_filter,
        bytes      : string_filter,
        bytearray  : string_filter,
        list       : array_filter,
        tuple      : array_filter,
        set        : set_filter,
        frozenset  : set_filter,
        dict       : dict_filter,
        type(lambda a:a) : function_filter,
    }
    _seen_override = set([
        type(None), bool, int, float, complex, str, bytes, frozenset
    ])

    if sys.version_info[0] < 3:
        filters[long]    = number_long_filter
        filters[unicode] = string_filter
        filters[xrange]  = range_filter
        _seen_override.add(long)
        _seen_override.add(unicode)
        _seen_override.add(xrange)
    else:
        filters[range] = range_filter
        _seen_override.add(range)

    def __init__(self, **kwargs):
        for key in kwargs:
            if not hasattr(self, key):
                raise TypeError(
                    "got an unexpected keyword argument '{:s}'".format(key)
                )
            elif (key == 'color'):
                self._init_color(kwargs[key])
            else:
                setattr(self, key, kwargs[key])
        if self.multiline:
            self._linebreak = "\n"
        else:
            self._linebreak = ' '
            self.indent     = 0
            self.index      = False
            self.align_hash = False
        self.stack = DataPrinterStack()

    def _init_color(self, color):
        if not isinstance(color, dict):
            raise TypeError('color must be dict')
        for c in color:
            if not c in self.color:
                raise ValueError("unknown color {:s}".format(c))
            elif not isinstance(color[c], DataPrinterColor):
                raise TypeError('value of color must be DataPrinterColor()')
            else:
                self.color[c] = color[c]

    def p(self, item):
        output = DataPrinterOutput(self.output)
        if str(self.colored) == 'auto':
            self.colored = output.isatty()
        dump = self.np(item)
        with output:
            print(dump, end=self.output_end)
        if (self.return_value == 'pass'):
            return item
        elif (self.return_value == 'void'):
            return None
        elif (self.return_value == 'dump'):
            return dump
        else:
            raise Warning('unknown return_value')
            return None

    def np(self, item):
        if str(self.colored) == 'auto':
            self.colored = not('ANSI_COLORS_DISABLED' in os.environ)
        if self.caller_info:
            self.stack.push('print', self.get_caller_message())
        self.stack.push('dump', item)
        dump = u''
        self._depth = 0
        self._current_indent = 0
        self._seen = {};
        self._path = [self.name]
        for (operation, i) in self.stack.process():
            if operation == 'dump':
                i_type = type(i)
                i_id   = id(i)
                if i_type not in self._seen_override:
                    if i_id in self._seen:
                        name = self.colorize('repeated', self._seen[i_id])
                        self.stack.unshift('print', name)
                        continue
                    self._seen[i_id] = ''.join(self._path)
                if i_type in self.filters:
                    self.filters[i_type](self, i)
                else:
                    self.stack.unshift('print', self.colorize('unknown', str(i)))
            elif operation == 'print':
                dump += i
            elif operation == 'set_depth':
                self._depth = i
            elif operation == 'set_indent':
                self._current_indent = i
            elif operation == 'set_path':
                self._path = i
            else:
                raise Warning('unknown operation ' + operation)
        return dump

    def get_caller_message(self):
        caller  = stack()[-1]
        message = self.caller_message.format(
            filename = caller[1],
            line     = caller[2],
            module   = caller[3],
        )
        return self.colorize('caller_info', message) + self._linebreak

    def colorize(self, color, string):
        if not self.colored:
            return string
        elif not color in self.color:
            raise ValueError("unknown color '{:s}'".format(color))
        else:
            return self.color[color].colored(string)

    def escape(self, string, color_key):
        if self.escape_chars == 'all':
            res = _full_escape(string)
            return self.colorize('escaped', res)
        res = u''
        for (s, escaped) in _escape_generator(
            string,
            self.escape_chars,
            self.print_escapes,
            self.scalar_quotes,
        ):
            res += self.colorize('escaped' if escaped else color_key, s)
        return res


