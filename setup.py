#!/usr/bin/env python
# coding: utf-8
from setuptools import setup, find_packages
from codecs import open
from os import path
import unittest
from dataprinter import VERSION

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()


def my_test_suite():
    test_loader = unittest.TestLoader()
    test_suite = test_loader.discover('t/', pattern='*.py', top_level_dir=here)
    return test_suite

setup(
    name='dataprinter',
    version=VERSION,

    description="Port of perl's Data::Printer to python",
    long_description=long_description,

    url='https://bitbucket.org/gragory/python-data-printer',

    author='Grigoriy Koudrenko',
    author_email='gragory.mail@gmail.com',

    license='GPLv3+',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Software Development :: Debuggers',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 3',
    ],
    keywords='ddp data dumper dump',

    packages=find_packages(exclude=['contrib', 'docs', 'tests']),

    install_requires=['termcolor'],

    py_modules=['dataprinter'],
    test_suite='setup.my_test_suite',
)
